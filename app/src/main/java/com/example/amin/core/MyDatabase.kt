package com.example.amin.core

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.amin.data.model.Converters
import com.example.amin.data.model.EnterModel
import com.example.amin.data.model.ExitModel
import com.example.amin.data.model.ItemModel
import com.example.amin.data.services.dao.EnterDao
import com.example.amin.data.services.dao.ExitDao
import com.example.amin.data.services.dao.ItemDao

@TypeConverters(Converters::class)
@Database(entities = [ItemModel::class, EnterModel::class, ExitModel::class],version = 1)
abstract class MyDatabase : RoomDatabase() {

    abstract fun itemDao() : ItemDao
    abstract fun enterDao() : EnterDao
    abstract fun exitDao() : ExitDao

}