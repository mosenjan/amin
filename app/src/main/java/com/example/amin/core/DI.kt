package com.example.amin.core

import android.app.Application
import androidx.room.Room
import com.example.amin.data.repository.HelloRepository
import com.example.amin.data.repository.HelloRepositoryImpl
import com.example.amin.data.repository.ItemRepository
import com.example.amin.data.services.dao.EnterDao
import com.example.amin.data.services.dao.ExitDao
import com.example.amin.data.services.dao.ItemDao
import com.example.amin.ui.viewmodel.ItemViewModel
import com.example.amin.ui.viewmodel.MyViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {

    // single instance of HelloRepository
    single<HelloRepository> { HelloRepositoryImpl() }
    single { ItemRepository() }

    // MyViewModel ViewModel
    viewModel { MyViewModel(get()) }
    viewModel { ItemViewModel(get()) }
}

val localModule = module {

    fun provideDatabase(application: Application): MyDatabase{
        return Room.databaseBuilder(application, MyDatabase::class.java,"myDB")
            .fallbackToDestructiveMigration().build()
    }

    fun provideItemDao(database: MyDatabase):ItemDao{
        return database.itemDao()
    }
    fun provideEnterDao(database: MyDatabase): EnterDao {
        return database.enterDao()
    }
    fun provideExitDao(database: MyDatabase): ExitDao {
        return database.exitDao()
    }

    single { provideDatabase(androidApplication()) }
    single { provideItemDao(get()) }
    single { provideEnterDao(get()) }
    single { provideExitDao(get()) }

}