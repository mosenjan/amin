package com.example.amin.data.repository

import com.example.amin.data.model.ItemModel
import com.example.amin.data.source.local.ItemLocalDataSource

class ItemRepository {

    private val itemLocalDataSource: ItemLocalDataSource = ItemLocalDataSource()

    fun insertItem(item: ItemModel) = itemLocalDataSource.insert(item)

    fun selectAll():List<ItemModel>{
        return itemLocalDataSource.selectAll()
    }

    fun search(txt: String):List<ItemModel>{
        return itemLocalDataSource.search(txt)
    }
}