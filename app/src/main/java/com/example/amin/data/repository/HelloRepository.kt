package com.example.amin.data.repository

interface HelloRepository {
    fun giveHello(): String = "hi"
}

class HelloRepositoryImpl() : HelloRepository {
    override fun giveHello() = "Hello Koin"
}
