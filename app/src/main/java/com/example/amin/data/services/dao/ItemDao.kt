package com.example.amin.data.services.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.amin.data.model.ItemModel

@Dao
interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item : ItemModel)

    @Query("SELECT * FROM item_Table")
    fun selectAll(): List<ItemModel>

    @Query("SELECT * FROM item_Table WHERE name LIKE :txt")
    fun search(txt: String): List<ItemModel>

    @Delete
    fun delete(item: ItemModel)

}