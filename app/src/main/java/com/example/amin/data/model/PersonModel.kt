package com.example.amin.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "person_Table")
data class PersonModel (
    @PrimaryKey(autoGenerate = true)
    val id:Int,
    val name:String,
    val tel:String,
    val address:String,
    val bankAccount:String
)