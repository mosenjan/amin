package com.example.amin.data.source.local

import androidx.lifecycle.LiveData
import com.example.amin.data.model.ItemModel
import com.example.amin.data.services.dao.ItemDao
import org.koin.core.KoinComponent
import org.koin.core.inject

class ItemLocalDataSource : KoinComponent {

    private val itemDao : ItemDao by inject()

    fun insert(item: ItemModel) = itemDao.insert(item)

    fun selectAll():List<ItemModel> = itemDao.selectAll()

    fun search(txt: String):List<ItemModel> = itemDao.search(txt)
}