package com.example.amin.data.services.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.amin.data.model.ExitModel

@Dao
interface ExitDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(v : ExitModel)

    @Query("SELECT * FROM exit_Table")
    fun selectAll():List<ExitModel>


}