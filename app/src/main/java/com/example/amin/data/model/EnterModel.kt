package com.example.amin.data.model

import androidx.room.*
import java.util.*

@Entity(
    tableName = "enter_Table", foreignKeys = arrayOf(
        ForeignKey(
            entity = ItemModel::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("itemId"),
            onDelete = ForeignKey.NO_ACTION
        )
    )
)
data class EnterModel(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val itemId: Int,
    val count: Float,
    val buyPrice: Int,
    val salePrice: Int? = null,
    val personId: Int? = null,
    val place:Int,
    val date: Date? = null
)

