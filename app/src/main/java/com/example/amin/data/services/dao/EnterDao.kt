package com.example.amin.data.services.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.amin.data.model.EnterModel

@Dao
interface EnterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(v : EnterModel)

    @Query("SELECT * FROM enter_Table")
    fun selectAll():List<EnterModel>


}