package com.example.amin.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "exit_Table",foreignKeys = arrayOf(
    ForeignKey(
        entity = EnterModel::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("enterId"),
        onDelete = ForeignKey.NO_ACTION
    )
))
data class ExitModel (
    @PrimaryKey(autoGenerate = true)
    val id:Int,
    val enterId: Int,
    val count:Float,
    val date: Date
)