package com.example.amin.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "item_Table")
data class ItemModel(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val name: String,
    val unit: String
)