package com.example.amin.data.source.local

import com.example.amin.data.model.ExitModel
import com.example.amin.data.services.dao.ExitDao
import org.koin.core.KoinComponent
import org.koin.core.inject

class ExitLocalDataSource: KoinComponent {

    private val exitDao : ExitDao by inject()

    fun insert(item: ExitModel) = exitDao.insert(item)

    fun selectAll():List<ExitModel> = exitDao.selectAll()

}