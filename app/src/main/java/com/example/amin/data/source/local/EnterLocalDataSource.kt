package com.example.amin.data.source.local

import com.example.amin.data.model.EnterModel
import com.example.amin.data.services.dao.EnterDao
import org.koin.core.KoinComponent
import org.koin.core.inject

class EnterLocalDataSource: KoinComponent {

    private val enterDao : EnterDao by inject()

    fun insert(item: EnterModel) = enterDao.insert(item)

    fun selectAll():List<EnterModel> = enterDao.selectAll()

}