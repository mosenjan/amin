package com.example.amin.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.amin.R
import com.example.amin.databinding.FragmentSelectBinding
import com.example.amin.ui.adapters.ItemAdapter
import com.example.amin.ui.viewmodel.ItemViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SelectFragment : Fragment() {
    private val itemViewModel: ItemViewModel by viewModel()
    private lateinit var binding: FragmentSelectBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select, container, false)



        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.also {
            binding.viewModel = itemViewModel
            binding.lifecycleOwner = this
        }
        val itemAdapter = ItemAdapter()

        binding.adapter = itemAdapter


        itemViewModel.resultSelectAll.observe(viewLifecycleOwner, {

            Log.e("asas", it.toString())

            it.let(itemAdapter::submitList)

        })


    }
}