package com.example.amin.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.amin.R
import com.example.amin.databinding.FragmentHomeBinding
import com.example.amin.ui.viewmodel.ItemViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {
    private val itemViewModel:ItemViewModel by viewModel()
    private lateinit var binding : FragmentHomeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)





        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.also {
            binding.viewModel = itemViewModel
            binding.lifecycleOwner = this
        }

        binding.btnInsertNew.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_homeFragment_to_insertItemFragment)
        }

        binding.btnStore.setOnClickListener {
            it.findNavController().navigate(R.id.action_homeFragment_to_searchFragment)
        }

    }
}