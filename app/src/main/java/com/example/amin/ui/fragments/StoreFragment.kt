package com.example.amin.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import com.example.amin.R
import com.example.amin.databinding.FragmentStoreBinding
import com.example.amin.ui.viewmodel.ItemViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class StoreFragment : Fragment() {

    private lateinit var binding: FragmentStoreBinding
    private val itemViewModel: ItemViewModel by viewModel()

   // val args: SearchFragment by navArgs() //val args: ConfirmationFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_store, container, false)





        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.also {
            binding.viewModel = itemViewModel
            binding.lifecycleOwner = this
        }

        //val id = args.id


    }

}