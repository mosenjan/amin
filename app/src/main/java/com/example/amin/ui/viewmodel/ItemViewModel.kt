package com.example.amin.ui.viewmodel

import androidx.lifecycle.*
import com.example.amin.data.model.ItemModel
import com.example.amin.data.repository.ItemRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*


class ItemViewModel(val itemRepository : ItemRepository) : ViewModel(){

    var name = MutableLiveData<String>()
    var count = MutableLiveData<String>()
    var unit = MutableLiveData<String>()
    var buyPrice = MutableLiveData<String>()
    var salePrice = MutableLiveData<String>()
    var storeProperty = MutableLiveData<String>()
    var shopProperty = MutableLiveData<String?>()
    var minProperty = MutableLiveData<String?>()
    var dateModify = MutableLiveData<Date?>()

    var resultSelectAll = MutableLiveData<List<ItemModel>>()
    var resultSearch = MutableLiveData<List<ItemModel>>()

    init {

    }

    fun insertItem(){
        viewModelScope.launch(Dispatchers.IO) {
            itemRepository.insertItem(ItemModel(name= name.value!!.toString(),
                 unit = unit.value!!
            ))
        }
    }

    fun selectAll(){
            viewModelScope.launch(Dispatchers.IO) {
                resultSelectAll.postValue(itemRepository.selectAll())
            }
        }

    fun search(txt: String){
        viewModelScope.launch(Dispatchers.IO) {
            resultSearch.postValue(itemRepository.search(txt))
        }
    }
}