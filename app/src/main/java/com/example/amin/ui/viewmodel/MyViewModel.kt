package com.example.amin.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.amin.data.repository.HelloRepository

class MyViewModel(val repo : HelloRepository) : ViewModel() {


    fun sayHello() = "${repo.giveHello()} from  $this"
}
