package com.example.amin.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.amin.R
import com.example.amin.databinding.FragmentInsertItemBinding
import com.example.amin.ui.viewmodel.ItemViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class InsertItemFragment : Fragment() {

    private val itemViewModel: ItemViewModel by viewModel()
    private lateinit var binding: FragmentInsertItemBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_insert_item, container, false)







        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.also {
            binding.viewModel = itemViewModel
            binding.lifecycleOwner = this
        }

        binding.insertBtn.setOnClickListener {
            itemViewModel.insertItem()
        }


    }

}