package com.example.amin.ui.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import com.example.amin.R
import com.example.amin.databinding.FragmentSearchBinding
import com.example.amin.ui.adapters.SearchAdapter
import com.example.amin.ui.viewmodel.ItemViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SearchFragment : Fragment() {
    private val itemViewModel: ItemViewModel by viewModel()
    lateinit var binding: FragmentSearchBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        // Inflate the layout for this fragment



        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.also {
            binding.viewModel = itemViewModel
            binding.lifecycleOwner = this
        }
        val searchAdapter = SearchAdapter()
        binding.adapter = searchAdapter





        itemViewModel.resultSearch.observe(viewLifecycleOwner,{
            it.let (searchAdapter::submitList)
            Log.i("asas",it.toString())
        })


        binding.txtSearch.setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE){
                val s = textView.text.toString()
                //textView.text = null
                itemViewModel.search("%$s%")
            }
            false
        }




    }


}