package com.example.amin.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.amin.data.model.ItemModel
import com.example.amin.databinding.AdapterItemBinding

class ItemAdapter : ListAdapter<ItemModel,ItemAdapter.ItemViewHolder>(Companion) {

    class ItemViewHolder(val binding: AdapterItemBinding) : RecyclerView.ViewHolder(binding.root)
    companion object: DiffUtil.ItemCallback<ItemModel>() {
        override fun areItemsTheSame(oldItem: ItemModel, newItem: ItemModel): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: ItemModel, newItem: ItemModel): Boolean = oldItem.id == newItem.id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = AdapterItemBinding.inflate(layoutInflater)


        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.binding.item = currentItem
        holder.binding.executePendingBindings()


    }
}